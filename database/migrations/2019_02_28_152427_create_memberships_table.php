<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMembershipsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('memberships', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->longText('bio')->nullable();
            $table->enum('online', ['Yes', 'No']);
            $table->string('website')->nullable();
            $table->string('phone')->nullable();
            $table->mediumText('address_one')->nullable();
            $table->mediumText('address_two')->nullable();
            $table->string('town')->nullable();
            $table->string('postcode')->nullable();
            $table->string('status')->default('Approve')->nullable();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users') ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('memberships');
    }
}
