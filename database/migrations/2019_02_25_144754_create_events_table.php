<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('user_id');
            $table->unsignedInteger('venue_id');
            $table->unsignedInteger('page_id');
            $table->string('title')->nullable();
            $table->longText('description')->nullable();
            $table->enum('event_group', ['Full WAC Event', 'Exeter', 'Totnes', 'Cornwall', 'Plymouth', 'Tavistock']);
            $table->enum('open_to', ['Public', 'Members Only', 'Local Group Members Only']);
            $table->date('date')->nullable();
            $table->timeTz('start_time')->nullable();
            $table->timeTz('end_time')->nullable();
            $table->integer('cost_for_member')->nullable();
            $table->integer('cost_for_non_member')->nullable();
            $table->enum('online_booking', ['Enable', 'Disable']);
            $table->enum('dietary_requirement', ['vegetarian', 'vegan', 'gluten free', 'dairy free', 'allergies', 'other']);
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreign('venue_id')->references('id')->on('venues')->onDelete('cascade');
            $table->foreign('page_id')->references('id')->on('pages')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}
