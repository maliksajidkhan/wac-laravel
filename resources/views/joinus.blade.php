<html>

<head>
  <!-- BEGIN HEADER -->
  @include('partials.head')
  <!-- END HEADER -->
</head>

<body class="pace-black  mac desktop pace-done">
  <div class="pace  pace-inactive">
    <div class="pace-progress" data-progress-text="100%" data-progress="99" style="width: 100%;">
      <div class="pace-progress-inner"></div>
    </div>
    <div class="pace-activity"></div>
  </div>
  <!-- BEGIN HEADER -->
  @include('partials.navbar')
  <!-- END HEADER -->
  <section class="m-t-100 sm-m-t-30">
    <!-- START CONTACT SECTION -->
    <section class="container container-fixed-lg p-t-40 p-b-80  sm-p-t-40 sm-p-b-20">
      <h4>Join WAC Now!<br></h4>
      @if (Session::has('message'))
      <div class="alert alert-info">
        <h5> {{ Session::get('message') }} </h5>
      </div>
      @endif
      <div class="col-md-8 order-md-1">
        <form class="needs-validation" role="form" action="{{ route('membership.store') }}" method="post" enctype="multipart/form-data" id="payment-form">
          {{ csrf_field() }}
          <div class="row">
            <div class="col-md-6 mb-3">
              <label for="firstName">First name</label>
              <input type="text" class="form-control" id="fname" required name="fname">
            </div>
            <div class="col-md-6 mb-3">
              <label for="lastName">Last name</label>
              <input type="text" class="form-control" id="lname" required name="lname">
            </div>
          </div>

          <div class="row">
            <div class="col-md-12 mb-3">
              <label for="bio">Bio Data</label>
              <textarea class="form-control" rows="5" id="bio" name="bio"></textarea>
            </div>
          </div>

          <div class="row">
            <div class="col-md-6 mb-3">
              <label for="email">Email</label>
              <input type="email" class="form-control" id="email" required name="email">
            </div>
            <div class="col-md-6 mb-3">
              <label for="password">Password</label>
              <input type="password" class="form-control" id="password" required name="password">
            </div>
          </div>
          <div class="row">
          <div class=" col-md-6 mb-3">
            <label for="photo">Profile's Photo</label>
            <input type="file" class="form-control" id="photo" name="photo">
          </div>
          <div class=" col-md-6 mb-3">
            <label for="website">Website</label>
            <input type="url" class="form-control" id="website" name="website">
          </div>
          </div>
          <div class="mb-3">
            <label for="phone">Contact Number</label>
            <input type="tel" class="form-control" id="phone" name="phone" required>
          </div>
          <div class="mb-3">
            <label for="address">Address Line 1</label>
            <input type="text" class="form-control" id="address" name="address">

          </div>

          <div class="mb-3">
            <label for="address2">Address Line 2 <span class="text-muted">(Optional)</span></label>
            <input type="text" class="form-control" id="address2" name="address2">
          </div>
          <div class="mb-3">
            <label for="town">Town</label>
            <input type="text" class="form-control" id="town" name="town">
          </div>
          <div class="mb-3">
            <label for="postcode">Postcode</label>
            <input type="text" class="form-control" id="postcode" name="postcode">
          </div>
          <div class="form-group">
                        <label for="card-element">Credit Card</label>
                        <div id="card-element">
                          <!-- a Stripe Element will be inserted here. -->
                      </div>

                      <!-- Used to display form errors -->
                      <div id="card-errors" role="alert"></div>
                    </div>

                    <div class="spacer"></div>
          <div class="d-block my-3">
            <div class="custom-control custom-checkbox">
              <input type="checkbox" class="custom-control-input" id="save-info" required name="aggreement">
              <label class="custom-control-label" for="save-info">I have read and agree to the members responsibilities
                (below):</label>
            </div>
          </div>

          <hr class="mb-4">
          <h4 class="mb-3">WAC Memebers Responsibilities</h4>
          <p>WAC is an organisation which relies on mutual trust and the willingness to share ourselves. All members
            are responsible for ensuring the confidentiality and safety that facilitates such trust and sharing. It is
            essential that every effort is made to maintain the confidentiality of the group.</p>
          <p>On joining WAC and ecruing your password protected account, you will have access to the Members Area
            containing the Memerbship list. Please note members details will be visible to other Members from this
            page. If you DO NOT agree to this please contact the Membership Secretary via the contact drop down fom to
            have your details removed from the list.</p>
          <p>The members’ names, addresses, e-mail and telephone numbers on the membership list may not be disclosed to
            anyone who is not a member of WAC. Use of the membership list for publicity for events other than those
            organised by WAC could contravene the Data Protection Act</p>
          <p>Any member who feels aggrieved or has a concern with another member may approach the Committee either
            directly or through the Local Group Contact.</p>
          <p>Each member is to ensure that the annual subscription is paid up to date.</p>
          <p>On leaving WAC it is the final responsibility of all members to return or destroy all membership lists and
            any documents including membership information.</p>
          <input type="submit" value="Register" class="btn btn-primary btn-lg btn-block">
        </form>
      </div>
    </section>
    <!-- END CONTACT SECION -->

    <!-- BEGIN SCRIPTS -->
    @include('partials.scripts')
    <!-- END SCRIPTS -->
  </section>
</body>

</html>