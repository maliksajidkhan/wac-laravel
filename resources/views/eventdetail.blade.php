<html>

<head>
    <!-- BEGIN HEADER -->
    @include('partials.head')
    <!-- END HEADER -->
</head>

<body class="pace-black  mac desktop pace-done">
    <div class="pace  pace-inactive">
        <div class="pace-progress" data-progress-text="100%" data-progress="99" style="width: 100%;">
            <div class="pace-progress-inner"></div>
        </div>
        <div class="pace-activity"></div>
    </div>
    <!-- BEGIN HEADER -->
    @include('partials.navbar')
    <!-- END HEADER -->
    <section class="m-t-100 sm-m-t-40">
        <section class="p-b-50 p-t-50">
            <div class="container">
                <div class="panel panel-transparent">
                    <div class="tab-content bg-white ">
                        <div class="row p-l-20 p-r-20 p-b-20 p-t-5 xs-no-padding">
                            <div class="col-md-11">
                                <div class="tab-pane active" id="tab3hellowWorld">
                                    
                                <!-- will be used to show any messages -->
                    @if (Session::has('message'))
                                    <div class="alert alert-info"><h5> {{ Session::get('message') }} </h5></div>
                    @endif
                    
                                        <h3>{!!$event->title!!}</h3>
                                        <ul class="list-inline">
                                            <li class="list-inline-item">Event Host:  <span class="badge badge-light">{{$event->user->name}} </span></li>
                                            <li class="list-inline-item">Venue Name:  <span class="badge badge-info">{{$event->venue->name}} </span> </li>
                                            <li class="list-inline-item">City located:  <span class="badge badge-success">{{$event->page->title}} </span></li>
                                        </ul>
                                    <br>
                                <div class="m-t-15">
                                       {!!$event->description!!}

                                </div>
                                    <br/>
                                    <div>
                                        <p>
                                            <b>Open To</b>: {!!$event->open_to!!}
                                        </p>
                                        <p>
                                            <b>Cost for Member</b>: £{!!$event->cost_for_member!!}
                                        </p>
                                        <p>
                                            <b>Cost for Non-Member</b>: £{!!$event->cost_for_non_member!!}
                                        </p>
                                        <p>
                                            <b>Date</b>: {!!$event->date!!}
                                        </p>
                                        <p>
                                            <b>Start Time</b>: {!!$event->start_time!!}
                                        </p>
                                        <p>
                                            <b>End Time</b>: {!!$event->end_time!!}
                                        </p>
                                        <p>
                                            <b>Venue</b>: {{$event->venue->name}} <br>{{$event->venue->address}}
                                        </p>
                                        <br>
                                        @php
                                        $date = new DateTime($event->date);
                                        $now = new DateTime();
                                        @endphp
                                        @if ($date < $now)
                                        @elseif($event->online_booking == 'Disable')
                                        <h1 class="alert alert-info">Booking is not necessary for this event.</h1>
                                        @elseif ($date < $now)
                                        
                                        @elseif ($event->cost_for_non_member > 0)
                                        <p>
                                                <form action="{{ route('appy') }}" method="POST">
                                                    {{ csrf_field() }}
                                                <input type="hidden" name="event_id" value="{{$event->id}}">
                                                    <script
                                                            src="https://checkout.stripe.com/checkout.js" class="stripe-button"
                                                            data-key="pk_test_ks9An34uZO8eHyZswCiQhIyQ"
                                                            data-name="W.A.C"
                                                            data-label = "Book Now"
                                                            data-description="Online Event Booking"
                                                            data-image="https://stripe.com/img/documentation/checkout/marketplace.png"
                                                            data-locale="auto">
                                                    </script>
                                                </form>
                                            </p>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    <!-- END SECION -->
    @include('partials.footer')
    <!-- BEGIN SCRIPTS -->
    @include('partials.scripts')
    <!-- END SCRIPTS -->
    </section>
</body>

</html>