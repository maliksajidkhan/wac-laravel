<html>

<head>
    <!-- BEGIN HEADER -->
    @include('partials.head')
    <!-- END HEADER -->
</head>

<body class="pace-black  mac desktop pace-done">
    <div class="pace  pace-inactive">
        <div class="pace-progress" data-progress-text="100%" data-progress="99" style="width: 100%;">
            <div class="pace-progress-inner"></div>
        </div>
        <div class="pace-activity"></div>
    </div>
    <!-- BEGIN HEADER -->
    @include('partials.navbar')
    <!-- END HEADER -->
    <section class="m-t-100 sm-m-t-35">
        <section class="p-b-50 p-t-35">
            <div class="container">
                <div class="panel panel-transparent">
                    <ul class="nav nav-tabs nav-tabs-simple nav-tabs-left bg-white" id="tab-3">
                        <li>
                                    {!!$page->name!!}
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content bg-white ">
                        <div class="row p-l-20 p-r-20 p-b-20 p-t-5 xs-no-padding">
                            <div class="col-md-11">
                                <div class="tab-pane active" id="tab3hellowWorld">
                                        {!!$page->description!!}
                                        <br><br><br>
                                        <table class="table table-bordered table-dark">
                                            <thead>
                                              <tr>
                                                <th scope="col">Date</th>
                                                <th scope="col">Events (click for details & booking)</th>
                                              </tr>
                                            </thead>
                              
                                            <tbody>
                                              @foreach ($events as $event)
                                              <tr>
                                              <th scope="row">{{$event->date}}</th>
                                                <td>
                                                  <a href="{{('/event/'.$event->id)}}">{{$event->title}}</a>
                                                  <br>{{$event->event_group}} Open to {{$event->open_to}}. Cost for Member: £{{$event->cost_for_member}}. Cost for Non-Member: £{{$event->cost_for_non_member}}. <span class="badge badge-success">{{$event->page->title}} </span> <span class="badge badge-info">{{$event->venue->name}} </span>
                                                </td>
                                              </tr>
                                              @endforeach
                                            </tbody>
                                          </table>
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    <!-- END SECION -->
    @include('partials.footer')
    <!-- BEGIN SCRIPTS -->
    @include('partials.scripts')
    <!-- END SCRIPTS -->
    </section>
    </body> </html>