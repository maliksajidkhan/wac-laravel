<section class="p-b-5 p-t-15 xs-p-b-65 bg-master-lightest">
        <div class="container">
        <div class="row">
          <div class="col-sm-6">
            <!--<img width="auto" height="100" src="{{ Request::is('home') ? 'storage/logo/'.cache('logo') : '../storage/logo/'.cache('logo') }}" class="logo inline m-r-50" alt="">-->
            <div class="m-t-10 ">
              <ul class="no-style fs-11 no-padding font-arial">
                <li class="inline no-padding"><a href="#" class=" text-master p-r-10 b-r b-grey">Home</a></li>
                <li class="inline no-padding"><a href="#" class="hint-text text-master p-l-10 p-r-10 b-r b-grey">Events</a></li>
                <li class="inline no-padding"><a href="#" class="hint-text text-master p-l-10 p-r-10 b-r b-grey">Contact</a></li>
                <li class="inline no-padding"><a href="#" class="hint-text text-master p-l-10 p-r-10 b-r b-grey">Dashboard</a></li>
                <li class="inline no-padding"><a href="https://www.colvus.co.uk" class="hint-text text-master p-l-10 p-r-10 xs-no-padding xs-m-t-10">Made by Colvus</a></li>
              </ul>
            </div>
          </div>
          
          <div class="m-t-10">
          <div class="col-sm-6 text-right font-arial sm-text-left">
            <p class="fs-11 muted m-t-5">{{ cache('footer') }}</p>
            </div>
            </div>
          </div>
        </div>
      </section>