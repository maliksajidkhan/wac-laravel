<section class="container-fluid b-t b-white">
        <div class="row">
            @foreach ($events as $event)
            <div class="col-md-2 text-center bg-success hover-push demo-story-block">
                    <div class="hover-backdrop" style="background:url({{asset('assets/images/feature_1.jpg')}}"></div>
                    <div class=" bottom-left bottom-right p-b-40">
                      <a href="{{('/event/'.$event->id)}}"><h5 class="text-white m-b-25">{{$event->title}}</h5></a>
                      <a class="font-montserrat fs-12 hint-text text-white all-caps" href="{{('/event/'.$event->id)}}">{{$event->page->title}}</a>
                    </div>
                  </div>
            @endforeach
        </div>
      </section>