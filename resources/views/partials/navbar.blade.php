<nav class="header md-header light-solid" data-pages="header" data-pages-header="autoresize" data-pages-resize-class="dark">
  <div class="container relative">
    <!-- BEGIN LEFT CONTENT -->
    <div class="pull-left">
      <!-- .header-inner
                  Allows to horizontally Align elements to the Center
                   -->
      <div class="header-inner">
        <!-- BEGIN LOGO -->
        <a href="{{url("/home")}}">

            <img src="{{ Request::is('home') ? 'storage/logo/'.cache('logo') : '../storage/logo/'.cache('logo') }}" class="p-l-5 p-r-5 p-t-15 p-b-5" alt="" width="auto" height="130">
          
          </a>

      </div>
    </div>
    <div class="pull-right">
      <div class="header-inner">
        <div class="visible-sm-inline visible-xs-inline menu-toggler pull-right p-l-10" data-pages="header-toggle"
          data-pages-element="#header">
          <div class="one"></div>
          <div class="two"></div>
          <div class="three"></div>
        </div>
      </div>
    </div>
    <!-- BEGIN RIGHT CONTENT -->
    <div class="menu-content pull-right clearfix" data-pages="menu-content" data-pages-direction="slideRight" id="header">
      <!-- BEGIN HEADER CLOSE TOGGLE FOR MOBILE -->
      <div class="pull-right">
        <a href="#" class="text-black link padding-10 visible-xs-inline visible-sm-inline pull-right m-t-10 m-b-10 m-r-10"
          data-pages="header-toggle" data-pages-element="#header">
          <i class=" pg-close_line"></i>
        </a>
      </div>
      <!-- END HEADER CLOSE TOGGLE FOR MOBILE -->
      <!-- BEGIN MENU ITEMS -->
      <div class="header-inner p-l-5 p-r-5 p-t-15 p-b-5">
        <ul class="menu">
          @foreach (cache('pages')  as $page)
          <li>
              <a href="{{url('page/'.$page->slug)}}" data-text="Home">{{$page->title}}
                <span data-text="Welcome">{{$page->sub_title}}</span>
              </a>
            </li>
          @endforeach
          <li class="classic  multiline">
            <a href="javascript:;" data-text="Elements">Groups <i class="pg-arrow_minimize m-l-5"></i>
              <span data-text="Shortcodes">Our locations</span></a>
            <nav class="classic ">
              <span class="arrow"></span>
              <ul>
                  @foreach (cache('groups')  as $group)
                  <li>
                      <li>
                          <a href="{{url('page/'.$group->slug)}}">{{$group->title}}</a>
                        </li>
                    </li>
                  @endforeach
              </ul>
            </nav>
          </li>
          <li>
            <a href="{{url("/event")}}" data-text="Events">Events
              <span data-text="Welcome">Coming up.</span>
            </a>
          </li>

          <li class="classic  multiline">
            <a href="javascript:;" data-text="Elements">Membership <i class="pg-arrow_minimize m-l-5"></i>
              <span data-text="Shortcodes">Members or Join us</span></a>
            <nav class="classic ">
              <span class="arrow"></span>
              <ul>
                <li>
                  <a href="{{url("/membership")}}">Join Us</a>
                </li>
                <li>
                  <a href="http://colvus.herokuapp.com/">Membership Login</a>
                </li>
              </ul>
            </nav>
          </li>

          <li>
            <a href="{{url("/contact")}}" data-text="Contact Us">Contact Us
              <span data-text="Get in touch">Get in touch</span></a>
          </li>
          <li>
            <a href="{{url("http://colvus.herokuapp.com")}}" data-text="Join Us">Dashboard
              <span data-text="Join us">Log in here</span></a>
          </li>
        </ul>

        <!-- BEGIN COPYRIGHT FOR MOBILE -->
        <div class="font-arial m-l-35 m-r-35 m-b-20 m-t-20 visible-sm visible-xs">
          <p class="fs-11 small-text muted">Copyright © 2018 Westcountry Association for Counselling</p>
        </div>
        <!-- END COPYRIGHT FOR MOBILE -->
      </div>
      <!-- END MENU ITEMS -->
    </div>
  </div>
</nav>