<html>

<head>
  <!-- BEGIN HEADER -->
  @include('partials.head')
  <!-- END HEADER -->
</head>

<body class="pace-black  mac desktop pace-done">
  <div class="pace  pace-inactive">
    <div class="pace-progress" data-progress-text="100%" data-progress="99" style="width: 100%;">
      <div class="pace-progress-inner"></div>
    </div>
    <div class="pace-activity"></div>
  </div>
  <!-- BEGIN HEADER -->
  @include('partials.navbar')
  <!-- END HEADER -->
  <section class="m-t-100 sm-m-t-30">
    <!-- BEGIN INTRO CONTENT -->
    @include('partials.events')
    <!-- END INTRO CONTENT -->
    <section class="p-b-50 p-t-50">
      <div class="container">
        <div class="panel panel-transparent">
          <ul class="nav nav-tabs nav-tabs-simple nav-tabs-left bg-white" id="tab-3">
          </ul>
          <div class="tab-content bg-white ">
            <div class="row p-l-20 p-r-20 p-b-20 p-t-5 xs-no-padding">
              <div class="col-md-11">
                <div class="tab-pane active" id="tab3hellowWorld">
                  {!!$home->description!!}
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- END SECION -->
    @include('partials.footer')
    <!-- BEGIN SCRIPTS -->
    @include('partials.scripts')
    <!-- END SCRIPTS -->
  </section>
</body>

</html>