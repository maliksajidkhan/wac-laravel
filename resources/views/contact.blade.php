<html>
  <head>
    <!-- BEGIN HEADER -->
@include('partials.head')
<!-- END HEADER -->
</head>
  <body class="pace-black  mac desktop pace-done"><div class="pace  pace-inactive"><div class="pace-progress" data-progress-text="100%" data-progress="99" style="width: 100%;">
  <div class="pace-progress-inner"></div>
</div>
<div class="pace-activity"></div></div>
    <!-- BEGIN HEADER -->
    @include('partials.navbar')
    <!-- END HEADER -->
    <section class="m-t-100 sm-m-t-230">
      <!-- START CONTACT SECTION -->
      <section class="container container-fixed-lg p-t-50 p-b-80  sm-p-t-30 sm-p-b-30">
<div class="row">
<div class="col-md-5">
<div class="row">
<div class="col-sm-6 col-md-12">
<h4>How can we assist you? <br></h4>
</div>
<div class="col-sm-6 col-md-12">

<p>If you wish to talk to someone regarding membership please use the form below to send a message to WAC. </p>
</div>
<div class="col-sm-6 col-md-12">
  <h4>Setting up a Local WAC group?</h4>
  </div>
  <div class="col-sm-6 col-md-12">

    <p>We are keen to set up new local groups accross the westcountry.  If you feel that you would like to start a group in Cornwall or North Devon, in particular, then please do get in touch.</p>
    <p>We are able to offer some financial support and it is easier than you may think to set up a group and someone will be there in the back ground to support you.</p>
  </div>
</div>
</div>
<div class="col-md-7">


<h4 class="sm-m-t-20">Contact us below</h4>
<!-- will be used to show any messages -->
  @if (Session::has('message'))
      <div class="alert alert-info"><h5> {{ Session::get('message') }} </h5></div>
  @endif
<form class="" role="form" action="{{ route('contact.store') }}" method="post">
  @csrf
<div class="row">
<div class="col-sm-6">
<div class="form-group form-group-default">
<label>First name</label>
<input type="text" class="form-control" name="fname" required>
</div>
</div>
<div class="col-sm-6">
<div class="form-group form-group-default">
<label>Last name</label>
<input type="text" class="form-control" name="lname" required>
</div>
</div>
</div>
<div class="form-group form-group-default">
  <label>Email</label>
  <input type="email" placeholder="Enter your email address" class="form-control" name="email" required>
  </div>
    <div class="form-group form-group-default">
        <label>Recipient</label>
        <select class="form-control" id="exampleFormControlSelect1" name="recipient" required>
          <option value="Vice Chair">Vice Chair</option>
          <option value="Chair">Chair</option>
          <option value="Treasurer">Treasurer</option>
          <option value="Membership Secretary">Membership Secretary</option>
          <option value="Secretary">Secretary</option>
          <option value="Journal & Newsletter Editor">Journal & Newsletter Editor</option>
          <option value="Web Coordinator">Web Coordinator</option>
          <option value="Exeter Representative">Exeter Representative</option>
          <option value="Plymouth Representative">Plymouth Representative</option>
          <option value="Tavistock Representative">Tavistock Representative</option>
          <option value="Cornwall Representative">Cornwall Representative</option>
        </select>
      </div>
    <div class="form-group form-group-default">
      <label>Subject</label>
      <input type="text" placeholder="What is this regarding?" class="form-control" name="subject" required>
      </div>
<div class="form-group form-group-default">
<label>Message</label>
<textarea class="form-control" style="height:100px" placeholder="Type the message you want inquire" name="message" required></textarea>
</div>
<p class="pull-left  small hint-text m-t-5 font-arial sm-m-t-10">I hereby certify that the information above is true and accurate. </p>
<button class="btn btn-white font-montserrat all-caps fs-11 pull-right sm-m-t-10" type="submit">Send request</button>
<div class="clearfix"></div>
</form>
</div>
</div>
</section>
    <!-- END SECION -->
    @include('partials.footer')
    <!-- BEGIN SCRIPTS -->
    @include('partials.scripts')
    <!-- END SCRIPTS -->
</section></body></html>
