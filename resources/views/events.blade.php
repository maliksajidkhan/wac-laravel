<html>

<head>
    <!-- BEGIN HEADER -->
    @include('partials.head')
    <!-- END HEADER -->
</head>

<body class="pace-black  mac desktop pace-done">
    <div class="pace  pace-inactive">
        <div class="pace-progress" data-progress-text="100%" data-progress="99" style="width: 100%;">
            <div class="pace-progress-inner"></div>
        </div>
        <div class="pace-activity"></div>
    </div>
    <!-- BEGIN HEADER -->
    @include('partials.navbar')
    <!-- END HEADER -->
    <section class="m-t-100 sm-m-t-40">
        <!-- START CONTACT SECTION -->
        <section class="container container-fixed-lg p-t-50 p-b-80  sm-p-t-30 sm-p-b-20">
            <h1>Events Calendar<br></h1>
            <P>When booking an event please be sure to sumbit dietary requirements in the ADDITI0NAL INFORMATION BOX, no later than 7 days before the event.
                <br> <br> If you are making bookings for more than 1 could you state the name of the 'other' attending in the ADDITIONAL INFORMATION BOX also, for CPD Certificate purposes. with much apprecieaation WAC.
                <P>
                    <div class="bd-example">
                        <table class="table table-bordered table-dark">
                            <thead>
                                <tr>
                                    <th scope="col">Date</th>
                                    <th scope="col">Events (click for details & booking)</th>
                                </tr>
                            </thead>

                            <tbody>
                                @foreach ($events as $event)
                                <tr>
                                    <th scope="row">{{$event->date}}</th>
                                    <td>
                                        <a href="{{('/event/'.$event->id)}}">{{$event->title}}</a>
                                        <br>{{$event->event_group}} Open to {{$event->open_to}}. Cost for Member: £{{$event->cost_for_member}}. Cost for Non-Member: £{{$event->cost_for_non_member}}. <span class="badge badge-success">{{$event->page->title}} </span>                                        <span class="badge badge-info">{{$event->venue->name}} </span>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    </div>
                    </div>
                    </div>
                    </div>
                    </div>
        </section>
        <!-- END SECION -->
        @include('partials.footer')
        <!-- BEGIN SCRIPTS -->
        @include('partials.scripts')
        <!-- END SCRIPTS -->
    </section>
</body>

</html>