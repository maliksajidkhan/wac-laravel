<?php
use Intervention\Image\Facades\Image;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('home');
});

Route::resource('contact','ContactController');
Route::resource('membership','MembershipController');
Route::resource('home','HomeController');
Route::resource('page','PageController');
Route::resource('event','EventController');

Route::post('/apply', 'ApplyController@apply')->name('appy');


Route::get('storage/profile/photos/{filename}', function ($filename) {
    return Image::make(storage_path('profile/photos/' . $filename))->response();
});

Route::get('storage/logo/{filename}', function ($filename) {
    return Image::make(storage_path('logo/' . $filename))->response();
});
