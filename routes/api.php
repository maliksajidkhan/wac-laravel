<?php
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */
Route::group([    
    'middleware' => 'api',    
    'prefix' => 'password'
], function () {    
    Route::post('store', 'API\PasswordResetController@store');
    Route::get('find/{token}', 'API\PasswordResetController@find');
    Route::post('reset', 'API\PasswordResetController@reset');
});
Route::group([    
    'middleware' => 'api'
], function () {    
    Route::get('signup/activate/{token}', 'AuthController@signupActivate');
});

Route::group(['middleware' => 'jwt.auth'], function () {
    Route::get('user/logout', 'API\AuthController@logout');
    Route::post('user/check-email', 'API\AuthController@checkEmail');
    Route::get('user/reset-password', 'API\AuthController@resetPassword');
    Route::post('user/password-reset', 'API\AuthController@setNewPassword');
    Route::put('user/profile-update/{id}', 'API\AuthController@profileUpdate');
    Route::get('roles', 'API\UserController@roles');
    Route::get('permissions', 'API\UserController@permissions');
    Route::apiResource('user', 'API\UserController');

    Route::get('group-pages', 'API\PageController@groupPages');
    Route::apiResource('page', 'API\PageController');
    
    Route::post('participants/{id}', 'API\ApplyController@participants');
    Route::post('/apply', 'API\ApplyController@apply');
    Route::get('/apply', 'API\ApplyController@index');

    Route::apiResource('event', 'API\EventController');

    Route::get('membership/dashboard-info', 'API\MembershipController@dashboardInfo');
    Route::get('membership/single', 'API\MembershipController@single');
    Route::put('membership/single/{id}', 'API\MembershipController@singleUpdate');
    Route::get('membership/single/{id}', 'API\MembershipController@profile');
    Route::post('membership/mailing-labels', 'API\MembershipController@mailingLabels');
    Route::put('membership-update/{id}', 'API\MembershipController@updateMember');
    Route::apiResource('membership', 'API\MembershipController');

    Route::apiResource('email', 'API\EmailController');
    Route::post('email/single/{email}', 'API\EmailController@singleEmail');
    
    Route::apiResource('autoresponse', 'API\AutoResponseController');

    Route::apiResource('venue', 'API\VenueController');

    Route::apiResource('setting', 'API\SettingController');
    
    Route::apiResource('role', 'API\RoleController');
});

Route::post('user/register', 'API\AuthController@register');
Route::post('user/login', 'API\AuthController@login');
