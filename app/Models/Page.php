<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id','title', 'sub_title','group_page','slug','number', 'meta_keywords', 'meta_description','description','banner_photo'
    ];

     /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'user','showDetails'
    ];

    /**
     * Get the user of the page.
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function event()
    {
        return $this->hasMany('App\Models\Event');
    }
}
