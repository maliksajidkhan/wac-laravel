<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'header','membership_cost', 'footer', 'facebook_page', 'facebook_group', 'twitter_url', 'logo'
    ];
}
