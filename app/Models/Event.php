<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'venue_id',
        'page_id',
        'title',
        'description',
        'event_group',
        'date',
        'start_time',
        'end_time',
        'cost_for_member',
        'cost_for_non_member',
        'open_to',
        'online_booking',
    ];

        /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
    ];

    /**
     * Get the user of the event.
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    /**
     * Get the venue of the event.
     */
    public function venue()
    {
        return $this->belongsTo('App\Models\Venue');
    }

    public function page()
    {
        return $this->belongsTo('App\Models\Page');
    }
}
