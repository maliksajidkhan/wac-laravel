<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Membership extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id','bio', 'website', 'phone',  'address_one', 'address_two','town','postcode'
    ];


    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
