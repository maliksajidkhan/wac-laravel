<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Venue extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'address', 'town', 'postcode', 'direction'
    ];

    /**
     * Get the event.
     */
    public function event()
    {
        return $this->hasMany('App\Models\Event');
    }
}
