<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;
use Laravel\Cashier\Billable;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use Notifiable;
    use HasRoles;
    use Billable;
    use SoftDeletes;
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'photo', 'name', 'type', 'email', 'password','active', 'activation_token'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'roles','activation_token'
    ];

    /**
     * Get the pages of the user.
     */
    public function pages()
    {
        return $this->hasMany('App\Models\Page');
    }

    /**
     * Get the events of the user.
     */
    public function events()
    {
        return $this->hasMany('App\Models\Event');
    }
   /**
     * Get the member.
     */
    public function member()
    {
        return $this->hasOne('App\Models\Membership');
    }
}
