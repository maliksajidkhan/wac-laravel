<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Apply;
use App\Models\Membership;
use App\User;
use App\Models\Event;
use Stripe\Charge;
use Stripe\Customer;
use Stripe\Stripe;
use Session;
use Redirect;

class ApplyController extends Controller
{
    public function apply(Request $request)
    {
        $event = Event::find($request->get('event_id'));
        try {
            Stripe::setApiKey(env('STRIPE_SECRET_KEY'));

            $event = Event::find($request->get('event_id'));

            $customer = Customer::create(array(
                'email' => $request->stripeEmail,
                'source'  => $request->stripeToken
            ));
    
            $charge = Charge::create(array(
                'customer' => $customer->id,
                'amount'   => $event->cost_for_non_member * 100,
                'currency' => 'GBP'
            ));

            // redirect
            Session::flash('message', 'You booked your seat successfully and ticket is sent to your email.');
            return Redirect::to('event/'.$request->get('event_id'));
        } catch (\Exception $ex) {
            return $ex->getMessage();
        }
    }
}
