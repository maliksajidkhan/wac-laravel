<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Page;
use App\Models\Event;
use App\Models\Setting;
use Cache;

class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $home = Page::where('id', 1)->first();

        $events = Event::orderBy('id', 'desc')->take(6)->get();

        $pages = Page::select('title', 'sub_title', 'slug', 'number')->where('group_page', 'No')->orderBy('number')->get();
        Cache::forever('pages', $pages);

        $groups = Page::select('title','slug', 'number')->where('group_page', 'Yes')->orderBy('number')->get();
        Cache::forever('groups', $groups);

        $settings = Setting::first();
        Cache::forever( 'header', $settings->header);
        Cache::forever( 'footer', $settings->footer);
        Cache::forever( 'logo', $settings->logo);

        return view('index', ['home' =>  $home, 'events' => $events]);

        // return view('index', ['home'=>$home, 'events'=>$events]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
