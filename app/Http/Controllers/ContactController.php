<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Contact;
use App\Models\AutoResponse;
use Redirect;
use Session;
use Mail;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('contact');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $response =  AutoResponse::first();
        $email = $request->get('email');
        $subject = $response->subject;
        $body = $response->message;
        
        Mail::send('welcome', ['subject' => $subject, 'body' => $body], function ($message) use ($email, $subject) {
            $message->to($email)
                ->from('admin@wacsouthwest.org.uk', 'Westcountry Association for Counselling')
                ->subject($subject);
        });

        $contact = new Contact([
            'fname' => $request->get('fname'),
            'lname'=> $request->get('lname'),
            'email'=> $request->get('email'),
            'recipient'=> $request->get('recipient'),
            'subject'=> $request->get('subject'),
            'message'=> $request->get('message'),
          ]);
          $contact->save();

          // redirect
            Session::flash('message', 'Email successfully sent to the WAC selected member.');
            return Redirect::to('contact');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
