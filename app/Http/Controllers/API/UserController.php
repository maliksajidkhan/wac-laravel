<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\User;
use Carbon\Carbon;
use DB;
use Illuminate\Http\Request;
use Image;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use Storage;
use Spatie\Permission\Contracts\Permission as SpatiePermission;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::where('type', 'User')->get();
        foreach ($users as $user) {
            $role = $user->getRoleNames();
            $user->role = $role[0];
        }
        return $users;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = User::findOrFail($id);
        $user->role = $user->getRoleNames();
        return $user;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'email' => 'required|email',
            'name' => 'required|string',
        ]);

        if ($request->get('photo') && $request->get('password')) {
            $imageData = $request->get('photo');
            $fileName = Carbon::now()->timestamp . '_' . uniqid() . '.' . explode('/', explode(':', substr($imageData, 0, strpos($imageData, ';')))[1])[1];
            Image::make($request->get('photo'))->save(storage_path('profile/photos/') . $fileName);

            $user = User::findOrFail($id);

            $role = $user->getRoleNames();
            $user->removeRole($role[0]);

            $inputs = array(
                'type' => 'User',
                'name' => $request->get('name'),
                'email' => $request->get('email'),
                'password' => bcrypt($request->get('password')),
                'photo' => $fileName,
            );

        } else if (!$request->get('photo') && !$request->get('password')) {
            $user = User::findOrFail($id);

            $role = $user->getRoleNames();
            $user->removeRole($role[0]);

            $inputs = array(
                'type' => 'User',
                'name' => $request->get('name'),
                'email' => $request->get('email'),
            );

        } else if ($request->get('photo')) {
            $imageData = $request->get('photo');
            $fileName = Carbon::now()->timestamp . '_' . uniqid() . '.' . explode('/', explode(':', substr($imageData, 0, strpos($imageData, ';')))[1])[1];
            Image::make($request->get('photo'))->save(storage_path('profile/photos/') . $fileName);

            $user = User::findOrFail($id);

            $role = $user->getRoleNames();
            $user->removeRole($role[0]);

            $inputs = array(
                'type' => 'User',
                'name' => $request->get('name'),
                'email' => $request->get('email'),
                'photo' => $fileName,
            );

        } else if ($request->get('password')) {
            $user = User::findOrFail($id);

            $role = $user->getRoleNames();
            $user->removeRole($role[0]);

            $inputs = array(
                'type' => 'User',
                'name' => $request->get('name'),
                'email' => $request->get('email'),
                'password' => bcrypt($request->get('password')),
            );
        } else {
            $user = User::findOrFail($id);

            $role = $user->getRoleNames();
            $user->removeRole($role[0]);

            $inputs = array(
                'type' => 'User',
                'name' => $request->get('name'),
                'email' => $request->get('email'),
            );
        }

        $user->fill($inputs)->save();

        $role = Role::findByName($request->get('role'));
        $user->assignRole($role);

        return response()->json(['user' => $user], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if (auth()->user()->id != $id) {
            $user = User::findOrFail($id);

            $role = $user->getRoleNames();
            $user->removeRole($role[0]);

            $user->delete();
            return response()->json(['msg' => 'User successfully Deleted'], 200);
        } else {
            return response()->json(['msg' => 'You are logged in with this account'], 403);
        }
    }

    /**
     * Return all roles
     *
     * @return \Illuminate\Http\Response
     */
    public function roles()
    {
        $roles = DB::table('roles')->select('name')->get();
        return $roles;
    }
    
    /**
     * Return all permissions
     *
     * @return \Illuminate\Http\Response
     */
    public function permissions()
    {
    
       $permissions = Permission::get();
        return $permissions;
    }
}
