<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Venue;

class VenueController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Venue::all();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'address' => 'required',
            'town' => 'required',
            'postcode' => 'required',
            'direction' => 'required'
        ]);


        $venue = Venue::create([
            'name' => $request->get('name'),
            'address' => $request->get('address'),
            'town' => $request->get('town'),
            'postcode' => $request->get('postcode'),
            'direction' => $request->get('direction')
        ]);

        return response(['page' => $venue], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $venue = Venue::find($id);

        return $venue;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required',
            'address' => 'required',
            'town' => 'required',
            'postcode' => 'required',
            'direction' => 'required'
        ]);

        $venue = Venue::findOrFail($id);
        $inputs = array('name' => $request->get('name'),
        'address' => $request->get('address'),
        'town' => $request->get('town'),
        'postcode' => $request->get('postcode'),
        'direction' => $request->get('direction'));

        $venue->fill($inputs)->save();

        return response()->json(['venue' => $venue], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $venue = Venue::find($id);
        $venue->delete();
        return response()->json(['msg' => 'Venue successfully Deleted'], 200);
    }
}
