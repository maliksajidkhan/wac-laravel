<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Event;
use App\Models\Apply;
use App\Models\Membership;
use Carbon;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $member = Membership::where('user_id', auth()->user()->id)->first();
        $events = Event::all();
        foreach ($events as $event) {
            // administrator
            $event->administrator = $event->user->name;

            // Venue
            $event->venuename = $event->venue->name;

            $event_id = $event->id;
            $count = Apply::where('event_id', $event_id)->count();
            $event->participants = $count;
            if($member)
            {
            $isapply = Apply::where('event_id', $event_id)->where('member_id',$member->id)->count();
            if($isapply == 1){
                $event->applied = true;  
            }else{
                $event->applied = false; 
            }
        }

        }

        return $events;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'administrator' => 'required',
            'title' => 'required|max:250',
            'venue_id' => 'required',
            'group_page_id' => 'required',
            'description' => 'required|min:50',
            'date' => 'required',
            'start_time' => 'required',
            'end_time' => 'required',
            'member_cost' => 'required',
            'non_member_cost' => 'required',
            'open_to' => 'required',
            'online_booking' => 'required'
        ]);


        $event = Event::create([
            'user_id' => $request->get('administrator'),
            'venue_id' => $request->get('venue_id'),
            'page_id' => $request->get('group_page_id'),
            'title' => $request->get('title'),
            'description' => $request->get('description'),
            'date' => $request->get('date'),
            'start_time' => date("G:i", strtotime($request->get('start_time'))),
            'end_time' => date("G:i", strtotime($request->get('end_time'))),
            'cost_for_member' => $request->get('member_cost'),
            'cost_for_non_member' => $request->get('non_member_cost'),
            'open_to' => $request->get('open_to'),
            'online_booking' => $request->get('online_booking'),
        ]);

        return response(['page' => $event], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $event = Event::find($id);
        $event->load('user','venue');

        return $event;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'administrator' => 'required',
            'title' => 'required|max:250',
            'venue_id' => 'required',
            'group_page_id' => 'required',
            'description' => 'required|min:50',
            'date' => 'required',
            'start_time' => 'required',
            'end_time' => 'required',
            'member_cost' => 'required',
            'non_member_cost' => 'required',
            'open_to' => 'required',
            'online_booking' => 'required',
            'venue_id' => 'required'
        ]);

        $event = Event::findOrFail($id);
        $inputs = array('user_id' => $request->get('administrator'),
        'title' => $request->get('title'),
        'venue_id' => $request->get('venue_id'),
        'page_id' => $request->get('group_page_id'),
        'description' => $request->get('description'),
        'date' => $request->get('date'),
        'start_time' => date("G:i", strtotime($request->get('start_time'))),
        'end_time' => date("G:i", strtotime($request->get('end_time'))),
        'cost_for_member' => $request->get('member_cost'),
        'cost_for_non_member' => $request->get('non_member_cost'),
        'open_to' => $request->get('open_to'),
        'online_booking' => $request->get('online_booking'),);

        $event->fill($inputs)->save();

        return response()->json(['event' => $event], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $event = Event::find($id);
        $event->delete();
        return response()->json(['msg' => 'Event successfully Deleted'], 200);
    }
    
}
