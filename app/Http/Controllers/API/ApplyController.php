<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Apply;
use App\Models\Membership;
use App\User;
use App\Models\Event;
use Illuminate\Http\Request;
use Stripe\Charge;
use Stripe\Customer;
use Stripe\Stripe;

class ApplyController extends Controller
{
    public function apply(Request $request)
    {
        $member = Membership::where('user_id', auth()->user()->id)->first();
        $event = Event::find($request->get('eventId'));
        try {
            Stripe::setApiKey(env('STRIPE_SECRET_KEY'));

            $customer = Customer::create(array(
                'email' => $request->email,
                'source' => $request->token,
          ));

             $charge = Charge::create(array(
                'customer' => $customer->id,
                'amount' => $event->cost_for_member * 100,
                'currency' => 'USD',
            ));

            $apply = Apply::create([
                'member_id' => $member->id,
                'event_id' => $request->get('eventId'),
                'date' => date("Y-m-d H:i:s"),
            ]);

            return response(['apply' => $apply], 200);
        } catch (\Exception $ex) {
            return $ex->getMessage();
        }
    }

    public function participants($id)
    {
        $applies = Apply::where('event_id', $id)->get();

        foreach ($applies as $apply) {
            $member_id = $apply->member_id;
            $member = Membership::where('id', $member_id)->first();

            $user_id = $member->user_id;
            $user = User::find($user_id);

            $apply->name = $user->name;
            $apply->email = $user->email;
            $apply->date = $apply->date;
        }

        return $applies;
    }

    public function index()
    {
        $all_applies = [];
        $applies = Apply::with('event', 'member')->get();

        
        foreach ($applies as $apply) {
            $title = $apply->event->title;
            $name = $apply->member->user->name;
            $email = $apply->member->user->email;
            $amount = $apply->event->cost_for_member;
            $date = $apply->date;

            $single_apply = ['title' => $title, 'name' => $name,'email' => $email, 'amount' => $amount, 'date' => $date];

            array_push($all_applies, $single_apply);
        }

        return $all_applies;
    }
}
