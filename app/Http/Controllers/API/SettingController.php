<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Setting;
use Cache;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Image;
use Storage;

class SettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Setting::first();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'header' => 'required',
            'membership_cost' => 'required',
            'footer' => 'required',
        ]);

        $setting = Setting::findOrFail($id);

        if($request->get('logo')){
            $imageData = $request->get('logo');
            $fileName = Carbon::now()->timestamp . '_' . uniqid() . '.' . explode('/', explode(':', substr($imageData, 0, strpos($imageData, ';')))[1])[1];
            Image::make($request->get('logo'))->save(storage_path('logo/') . $fileName);

            $inputs = array('header' => $request->header, 'footer' => $request->footer, 'logo' => $fileName, 'facebook_page' => $request->facebook_page, 'facebook_group' => $request->facebook_group, 'twitter_url' => $request->twitter_url, 'membership_cost' => $request->membership_cost);
        }else{
            $inputs = array('header' => $request->header, 'footer' => $request->footer, 'facebook_page' => $request->facebook_page, 'facebook_group' => $request->facebook_group, 'twitter_url' => $request->twitter_url, 'membership_cost' => $request->membership_cost);
        }
        

        

        $setting->fill($inputs)->save();

        // Refresh cache
        Cache::forget('header');
        Cache::forget('footer');
        Cache::forget('logo');
        Cache::forget('facebook_page');
        Cache::forget('facebook_group');
        Cache::forget('twitter_url');

        Cache::forever('header', $setting->header);
        Cache::forever('footer', $setting->footer);
        Cache::forever('logo', $setting->logo);
        Cache::forever('facebook_page', $setting->facebook_page);
        Cache::forever('facebook_group', $setting->facebook_group);
        Cache::forever('twitter_url', $setting->twitter_url);

        return response()->json(['setting' => $setting], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
