<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Membership;
use App\Models\Event;
use App\User;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;
use DateTime;
use Carbon\Carbon;
use Image;
use Storage;
use Mail;

class MembershipController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $memberships = Membership::all();
        foreach ($memberships as $membership) {
            $user_id = $membership->user_id;

            // Append User with Membership
            $user = User::where('id', $user_id)->first();
            $membership->name = $user->name;
            $membership->email = $user->email;

            // Append Role with Membership
            $role = $user->getRoleNames();
            $membership->role = $role[0];
        }
        return $memberships;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $imageData = $request->get('photo');
        $fileName = Carbon::now()->timestamp . '_' . uniqid() . '.' . explode('/', explode(':', substr($imageData, 0, strpos($imageData, ';')))[1])[1];
        Image::make($request->get('photo'))->save(storage_path('profile/photos/').$fileName);


        $user = User::create([
            'name' => $request->get('fname') . ' ' . $request->get('lname'),
            'email' => $request->get('email'),
            'password' => bcrypt($request->get('password')),
            'type' => 'Member',
            'photo' => $fileName,
             'activation_token' => str_random(60)
        ]);
        $user->notify(new SignupActivate($user));

        $email= $request->get('email');
        $subject= "Westcountry Association for Counselling";

        Mail::send('emails/register',['name' => $request->get('fname') . ' ' . $request->get('lname'), 'email' =>$request->get('email'), 'role' =>'Member','password' => $request->get('password')], function ($message) use ($email, $subject) {
            $message->to($email)
                ->from('admin@wacsouthwest.org.uk', 'Westcountry Association for Counselling')
                ->subject($subject);
        });

        $role = Role::findByName($request->get('role'));
        $user->assignRole($role);

        $membership = new Membership([
            'user_id' => $user->id,
            'bio' => $request->get('bio'),
            'website' => $request->get('website'),
            'phone' => $request->get('contact_no'),
            'address_one' => $request->get('address_one'),
            'address_two' => $request->get('address_two'),
            'town' => $request->get('town'),
            'postcode' => $request->get('postcode'),
        ]);

        $membership->save();
        return "Saved";
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $member = Membership::findOrFail($id);
        $member->load('user');
        
        $user = User::find($member->user_id);
        
        $role = $user->getRoleNames();
        $member->role = $role[0];
        
        return $member;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $membership = Membership::findOrFail($id);

        $membership->status = $request->status;
        $membership->save();

        return response()->json(['membership' => $membership], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
            $member = Membership::find($id);
            $member->delete();
            return response()->json(['msg' => 'Member successfully Deleted'], 200);
    }

    public function single()
    {
        $user = auth()->user();
        $user->load('member');

        return $user;
    }

    public function singleUpdate(Request $request, $id)
    {
        
             if(!empty($request->get('photo'))){
             $imageData = $request->get('photo');
        $fileName = Carbon::now()->timestamp . '_' . uniqid() . '.' . explode('/', explode(':', substr($imageData, 0, strpos($imageData, ';')))[1])[1];
        Image::make($request->get('photo'))->save(storage_path('profile/photos/').$fileName);
        }

        $user = User::findOrFail($id);
        $user_id = $user->id;
        $user->name = $request->name;
        $user->email = $request->email;
         if($request->get('photo')){
          $user->photo = $fileName;  
        }
        $user->save();

        $member = Membership::where('user_id',$user_id)
        ->update([
            'bio' => $request->bio,
            'website' => $request->website,
            'phone' => $request->phone,
            'address_one' => $request->address_one,
            'address_two' => $request->address_two,
            'town' => $request->town,
            'postcode' => $request->postcode,
        ]);
        return response()->json(['member' => $member], 200);
    }

    public function profile($id)
    {
        $membership = Membership::findOrFail($id);
        $membership->bio;
        $membership->load('user');
        return $membership;
    }

    public function dashboardInfo()
    {
        $membersCount = Membership::count();
        $membersOnlineCount = Membership::where('online', 'Yes')->count();

        $EventsCount = Event::count();
        $now = new DateTime();
        $upComingEventsCount = Event::where('date', '>' ,$now)->count();


        return response()->json(['membersCount' => $membersCount, 'membersOnlineCount'=>$membersOnlineCount, 'eventsCount'=>$EventsCount, 'upComingEventsCount'=>$upComingEventsCount], 200);
    }

    public function mailingLabels(Request $request)
    {
         $member_ids = $request->members;

         return $membership = Membership::with('user')->whereIn('id', $member_ids)->get();
    }

    public function updateMember(Request $request, $id)
    {
        $membership = Membership::findOrFail($id);
        $user = $membership->user;
        if($request->get('role')){
            $role = $user->getRoleNames();
            $user->removeRole($role[0]);
            
            $role = Role::findByName($request->get('role'));
            $user->assignRole($role);
        }
        $membership->bio = $request->bio;
        $membership->phone = $request->contact_no;
        $membership->website = $request->website;
        $membership->address_one = $request->address_one;
        $membership->address_two = $request->address_two;
        $membership->town = $request->town;
        $membership->postcode = $request->postcode;

        $membership->save();

        $user = User::findOrFail($membership->user_id);

        if($request->get('photo') && $request->get('password')){
            $imageData = $request->get('photo');
            $fileName = Carbon::now()->timestamp . '_' . uniqid() . '.' . explode('/', explode(':', substr($imageData, 0, strpos($imageData, ';')))[1])[1];
            Image::make($request->get('photo'))->save(storage_path('profile/photos/').$fileName);
            
            $inputs = array(
                'type' => 'User',
                'name' => $request->get('fname').' '.$request->get('lname'),
                'email' => $request->get('email'),
                'password' => bcrypt($request->get('password')),
                'photo' => $fileName
            );
        }

        if($request->get('photo')){
            $imageData = $request->get('photo');
            $fileName = Carbon::now()->timestamp . '_' . uniqid() . '.' . explode('/', explode(':', substr($imageData, 0, strpos($imageData, ';')))[1])[1];
            Image::make($request->get('photo'))->save(storage_path('profile/photos/').$fileName);
            
            $inputs = array(
                'type' => 'User',
                'name' => $request->get('fname').' '.$request->get('lname'),
                'email' => $request->get('email'),
                'photo' => $fileName
            );
        }

        if($request->get('password')){
            $inputs = array(
                'type' => 'User',
                'name' => $request->get('fname').' '.$request->get('lname'),
                'email' => $request->get('email'),
                'password' => bcrypt($request->get('password')),
            );
        }

        
        if(!$request->get('photo') && !$request->get('password')){ 
            $inputs = array(
                'type' => 'User',
                'name' => $request->get('fname').' '.$request->get('lname'),
                'email' => $request->get('email')
            );
        }

        $user->fill($inputs)->save();

        return response()->json(['user' => $user], 200);
    }
}
