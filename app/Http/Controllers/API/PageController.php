<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Page;
use App\User;
use Illuminate\Http\Request;
use Response;
use Spatie\Permission\Models\Role;
use Carbon\Carbon;
use Image;
use Storage;
use DateTime;

class PageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pages = Page::all();
        foreach ($pages as $page) {
            $page->username = $page->user->name;
        }

        return $pages;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'group_page' => 'required',
            'title' => 'required|max:250',
            'sub_title' => 'required|max:20',
            'number' => 'required',
            'meta_keywords' => 'required',
            'meta_description' => 'required|max:80',
            'description' => 'required|min:50',
        ]);

        $slug = str_replace(' ', '-', $request->get('title'));
        $slug = preg_replace('/[^A-Za-z0-9\-]/', '', $slug);

        if ($request->get('group_page') == 'Yes') {
            $role = $request->title . ' ' . 'Representative';
            Role::create(['name' => $role]);
           
        }
        if(!empty($request->get('banner_photo')))
        {
             $imageData = $request->get('banner_photo');
            $fileName = Carbon::now()->timestamp . '_' . uniqid() . '.' . explode('/', explode(':', substr($imageData, 0, strpos($imageData, ';')))[1])[1];
          Image::make($request->get('banner_photo'))->save(storage_path('banner/').$fileName);
          $request->merge(['banner_photo' => $fileName]);
        }

        $page = Page::create([
            'user_id' => auth()->user()->id,
            'group_page' => $request->get('group_page'),
            'slug' => $slug,
            'number' => $request->get('number'),
            'title' => $request->get('title'),
            'sub_title' => $request->get('sub_title'),
            'meta_keywords' => $request->get('meta_keywords'),
            'meta_description' => $request->get('meta_description'),
            'description' => $request->get('description'),
            'banner_photo' => $request->get('banner_photo')
        ]);

        return response(['page' => $page], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Page::find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $request->validate([
            'title' => 'required|max:250',
            'sub_title' => 'required|max:20',
            'group_page' => 'required',
            'number' => 'required',
            'meta_keywords' => 'required',
            'meta_description' => 'required|max:80',
            'description' => 'required|min:50',
        ]);

        $slug = str_replace(' ', '-', $request->get('title'));
        $slug = preg_replace('/[^A-Za-z0-9\-]/', '', $slug);

        $page = Page::findOrFail($id);

        if ($request->get('group_page') == 'Yes') {
            $role_name = $page->title . ' Representative';
            $role = Role::where(['name' => $role_name])->first();
            $role->name = $request->title. ' Representative';

            $role->save();
        }
         if(!empty($request->get('banner_photo')))
        {
             $imageData = $request->get('banner_photo');
            $fileName = Carbon::now()->timestamp . '_' . uniqid() . '.' . explode('/', explode(':', substr($imageData, 0, strpos($imageData, ';')))[1])[1];
          Image::make($request->get('banner_photo'))->save(storage_path('banner/').$fileName);
          $request->merge(['banner_photo' => $fileName]);
        }

        $inputs = array('title' => $request->title, 'sub_title' => $request->sub_title, 'group_page' => $request->group_page, 'number' => $request->number, 'slug' => $slug, 'meta_keywords' => $request->meta_keywords, 'meta_description' => $request->meta_description, 'description' => $request->description, 'banner_photo' => $request->banner_photo);

        $page->fill($inputs)->save();

        return response()->json(['page' => $page], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $page = Page::find($id);

        if ($page->group_page == 'Yes') {
            $role_name = $page->title . ' Representative';
            $role = Role::where(['name' => $role_name])->first();

            $role->delete();
        }
        $page->delete();
        return response()->json(['msg' => 'Event successfully Deleted'], 200);
    }

    public function groupPages()
    {
        return Page::where('group_page', 'Yes')->orderBy('title')->get();
    }
}
