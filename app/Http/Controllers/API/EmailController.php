<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Jobs\MultipleEmailJob;
use App\Models\Contact;
use App\Models\Membership;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Mail;
use Response;

class EmailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = auth()->user();

        $role = $user->getRoleNames();

        $contacts = Contact::where('recipient', $role[0])->orderBy('id', 'desc')->take(50)->get();
        return $contacts;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $members = Membership::all();
        $members->load('user');

        $emailJob = (new MultipleEmailJob($members, $request->subject, $request->body))->delay(Carbon::now()->addSeconds(50));
        dispatch($emailJob);

        return Response::json(['msg' => 'Sent Successfully'], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $contact = Contact::find($id);
        $contact->delete();
        return response()->json(['msg' => 'Email successfully Deleted'], 200);
    }

    public function singleEmail(Request $request, $email)
    {
        $subject = $request->subject;
        $body = $request->body;

        Mail::send('welcome', ['subject' => $subject, 'body' => $body], function ($message) use ($email, $subject) {
            $message->to($email)
                ->from('admin@wacsouthwest.org.uk', 'Westcountry Association for Counselling')
                ->subject($subject);
        });

        return Response::json(['msg' => 'Sent Successfully'], 200);
    }
}
