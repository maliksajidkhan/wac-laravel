<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;
use JWTAuth;
use Mail;
use Response;
use Spatie\Permission\Models\Role;
use App\Notifications\SignupActivate;
use Auth;
use App\Models\Membership;
use Carbon\Carbon;
use Image;
use Storage;

class AuthController extends Controller
{
    public function register(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
            'name' => 'required|string',
            'password' => 'required',
            'photo' => 'required',
        ]);

        $imageData = $request->get('photo');
        $fileName = Carbon::now()->timestamp . '_' . uniqid() . '.' . explode('/', explode(':', substr($imageData, 0, strpos($imageData, ';')))[1])[1];
        Image::make($request->get('photo'))->save(storage_path('profile/photos/').$fileName);

        $user = User::create([
            'type' => 'User',
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'password' => bcrypt($request->get('password')),
            'photo' => $fileName,
            'activation_token' => str_random(60)
        ]);
        $user->notify(new SignupActivate($user));
        $role = Role::findByName($request->get('role'));
        $user->assignRole($role);

        $email= $request->get('email');
        $subject= "Westcountry Association for Counselling";

        Mail::send('emails/register',['name' => $request->get('name'), 'email' =>$request->get('email'), 'role' =>$request->get('role'),'password' => $request->get('password')], function ($message) use ($email, $subject) {
            $message->to($email)
                ->from('admin@wacsouthwest.org.uk', 'Westcountry Association for Counselling')
                ->subject($subject);
        });

        $token = JWTAuth::fromUser($user);

        // Mail::to($request->get('email'))->send(new RegisterMailable($request->get('name')));

        return Response::json(['token' => $token], 200);
    }
    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|string|email|max:255',
            'password' => 'required',
        ]);
        $credentials = request(['email', 'password']);
        $credentials['active'] = 1;
        $credentials['deleted_at'] = null;
        try {
            if (!$token = JWTAuth::attempt($credentials)) {
                return response()->json(['error' => 'invalid_credentials'], 401);
            }
        } catch (JWTException $e) {
            return response()->json(['error' => 'could_not_create_token'], 500);
        }

        $user = Auth::user();
        if($user->type == 'Member')
        {
            $membership = Membership::where('user_id', $user->id)->first();
            $membership->online = 'Yes';
            $membership->save();
        }

        $roles = $user->getRoleNames();
$permissions = $user->getAllPermissions();
       
        $user->role = $roles[0];
        return response()->json(['token' => $token, 'user'=> $user, 'permissions'=>$permissions], 200);
    }

    public function logout()
    {
        JWTAuth::invalidate();
        return response([
            'status' => 'success',
            'msg' => 'Logged out Successfully.',
        ], 200);
    }

    /**
     * Find email in the storage
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function checkEmail(Request $request)
    {
        $request->validate([
            'email' => 'required',
        ]);

        $user = User::where('email', $request->get('email'))->count();
        if ($user == 0) {
            return response([
                'msg' => 'This email is available!',
            ], 200);
        } else {
            return response([
                'msg' => 'This email is taken by another user!',
            ], 302);
        }

    }

    public function resetPassword()
    {
        $subject= "Westcountry Association for Counselling";
        $user = auth()->user();
        $email = $user->email;

        Mail::send('emails/resetpassword',['name' => $user->name, 'email' => $email], function ($message) use ($email, $subject) {
            $message->to($email)
                ->from('admin@wacsouthwest.org.uk', 'Westcountry Association for Counselling')
                ->subject($subject);
        });

        return response([
            'msg' => 'Email is sent!',
        ], 200);
        
    }

    public function signupActivate($token)
    {
       $user = User::where('activation_token', $token)->first();
       if (!$user) {
        return response()->json([
            'message' => 'This activation token is invalid.'
        ], 404);
        }
          $user->active = true;
          $user->activation_token = '';
          $user->save();
         return $user;
    }

    public function setNewPassword(Request $request)
    {
        $id = auth()->user()->id;

        $user = User::find($id);

        $input = array('password' => bcrypt($request->password));

        $user->fill($input)->save();

        return response()->json(['user' => $user], 200);
    }
    
    public function profileUpdate(Request $request, $id){
        if(!empty($request->get('photo'))){
             $imageData = $request->get('photo');
        $fileName = Carbon::now()->timestamp . '_' . uniqid() . '.' . explode('/', explode(':', substr($imageData, 0, strpos($imageData, ';')))[1])[1];
        Image::make($request->get('photo'))->save(storage_path('profile/photos/').$fileName);
        }
        


        $user = User::findOrFail($id);
        $user_id = $user->id;

        // Update user table
        $user->name = $request->name;
        $user->email = $request->email;
        if($request->get('photo')){
          $user->photo = $fileName;  
        }
        $user->save();
        
        return response()->json(['user' => $user], 200);
    }
}
