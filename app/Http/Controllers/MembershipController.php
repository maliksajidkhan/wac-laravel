<?php

namespace App\Http\Controllers;

use App\Models\Membership;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Redirect;
use Session;
use Spatie\Permission\Models\Role;
use Stripe\Charge;
use Stripe\Customer;
use Stripe\Stripe;

class MembershipController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('joinus');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $photo = $request->file('photo');
        $extension = $photo->getClientOriginalExtension();

        $photo_name = strtotime("now") . $photo->getClientOriginalName() . '.' . $extension;
        Storage::disk('public')->put($photo_name, File::get($photo));

        $user = User::create([
            'name' => $request->get('fname') . ' ' . $request->get('lname'),
            'photo' => $photo_name,
            'email' => $request->get('email'),
            'password' => bcrypt($request->get('password')),
            'type' => 'Member',
        ]);
        

        $user->newSubscription('Annual Membership', 'plan_EtI6Pmzgg0AqZx')->create($request->get('stripeToken'));

        $role = Role::findByName('Member');
        $user->assignRole($role);

        $membership = new Membership([
            'user_id' => $user->id,
            'bio' => $request->get('bio'),
            'website' => $request->get('website'),
            'phone' => $request->get('phone'),
            'address_one' => $request->get('address'),
            'address_two' => $request->get('address2'),
            'town' => $request->get('town'),
            'postcode' => $request->get('postcode'),
        ]);

        $membership->save();

        // redirect
        Session::flash('message', 'Membership account is created successfully. Now you can login');
        return Redirect::to('membership');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    
}
